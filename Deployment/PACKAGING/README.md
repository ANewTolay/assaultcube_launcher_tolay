When packaging add the README.html and sha1.txt files from this directory to the compressed archive. Also include the AC_Launcher desktop file to Linux packages. 

In Windows use [7-zip](http://7-zip.org/) with the ultra compression setting. 

Note that Windows builds are compiled by MinGW for 32bit and require these extra libraries from a 32bit windows system (Or let the user know to download MinGW):
```
libgcc_s_dw2-1.dll
libstdc++-6.dll
libwinpthread-1.dll
```

In OSX use [Keka](http://www.kekaosx.com/) with the higest compression level of 7zip. 

In Linux install p7zip-full from the package manager and use lzma2 with the highest compression:
```
7z a -t7z -m0=lzma2 -mx=9 -mfb=128 -md=80m -ms=on Archive.7z /path/to/directory
```
