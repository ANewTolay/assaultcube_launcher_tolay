Run the script deploy.sh to convert QT 5+ releases to an application package.

http://stackoverflow.com/questions/27952111/unable-to-sign-app-bundle-using-qt-frameworks-on-os-x-10-10

As for calling the script:
```
# Define environment variables
export QT_BIN_PATH=/Path/To/Qt_5.6.2/5.6/clang_64/bin/
export QT_FRAMEWORK_PATH=/Path/To/Qt_5.6.2/5.6/clang_64/lib/
export CERTIFICATE="Developer ID Application: My Certificate"
export FRAMEWORKS="QtCore QtGui QtPrintSupport QtWidgets"
export BAD_FRAMEWORKS="QtPrintSupport"
```

```
# Call itself
deploy.sh SimpleHello.app
```

With this script, the final output is:
```
SimpleHello.app/: accepted
source=Developer ID
origin=Developer ID Application: My Certificate (HASH)
```
