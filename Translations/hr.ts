<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hr" sourcelanguage="en">
<context>
    <name>Add</name>
    <message>
        <location filename="../add.cpp" line="81"/>
        <location filename="../add.cpp" line="90"/>
        <location filename="../add.cpp" line="97"/>
        <source>Open AssaultCube</source>
        <translation>Otvori AssaultCube</translation>
    </message>
</context>
<context>
    <name>Edit</name>
    <message>
        <location filename="../edit.cpp" line="108"/>
        <location filename="../edit.cpp" line="124"/>
        <source>Open Game Path</source>
        <translation>Otvori lokaciju igre</translation>
    </message>
    <message>
        <location filename="../edit.cpp" line="117"/>
        <source>Open AssaultCube</source>
        <translation>Otvori AssaultCube</translation>
    </message>
</context>
<context>
    <name>add</name>
    <message>
        <location filename="../add.ui" line="14"/>
        <source>Launcher Add</source>
        <translation>Dodaj pokretač</translation>
    </message>
    <message>
        <location filename="../add.ui" line="74"/>
        <source>Path</source>
        <translation>Lokacija</translation>
    </message>
    <message>
        <location filename="../add.ui" line="94"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="../add.ui" line="112"/>
        <source>Options:</source>
        <translation>Opcije:</translation>
    </message>
    <message>
        <location filename="../add.ui" line="130"/>
        <source>Name:</source>
        <translation>Ime:</translation>
    </message>
</context>
<context>
    <name>edit</name>
    <message>
        <location filename="../edit.ui" line="14"/>
        <source>Launcher Edit</source>
        <translation>Uredi pokretač</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="74"/>
        <source>Path</source>
        <translation>Lokacija</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="94"/>
        <source>Save</source>
        <translation>Spremi</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="112"/>
        <source>Options:</source>
        <translation>Opcije:</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="130"/>
        <source>Name:</source>
        <translation>Ime:</translation>
    </message>
</context>
<context>
    <name>launcher</name>
    <message>
        <location filename="../launcher.ui" line="14"/>
        <source>QT AssaultCube Launcher</source>
        <translation>QT AssaultCube Pokretač</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="35"/>
        <source>Add +</source>
        <translation>Dodaj +</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="69"/>
        <source>Edit =</source>
        <translation>Uredi =</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="89"/>
        <source>Play &gt;</source>
        <translation>Igraj &gt;</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="106"/>
        <source>Chat on IRC</source>
        <translation>Chat na IRC</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="126"/>
        <source>Delete -</source>
        <translation>Izbriši -</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="405"/>
        <source>Sites</source>
        <translation>Web Stranice</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="413"/>
        <source>Documents</source>
        <translation>Dokumenti</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="420"/>
        <source>Content</source>
        <translation>Sadržaj</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="427"/>
        <source>Packages</source>
        <translation>Paketi</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="435"/>
        <source>About</source>
        <translation>O nama</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="453"/>
        <source>Docs</source>
        <translation>Dokumenti</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="458"/>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="463"/>
        <source>Gema Forums</source>
        <translation>Gema Forumi</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="468"/>
        <source>Iguana Site</source>
        <translation>Iguanina Web stranica</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="498"/>
        <source>Launcher</source>
        <translation>Pokretač</translation>
    </message>
</context>
</TS>
