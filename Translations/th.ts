<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="th" sourcelanguage="en">
<context>
    <name>Add</name>
    <message>
        <location filename="../add.cpp" line="81"/>
        <location filename="../add.cpp" line="90"/>
        <location filename="../add.cpp" line="97"/>
        <source>Open AssaultCube</source>
        <translation>เปิด AssaultCube</translation>
    </message>
</context>
<context>
    <name>Edit</name>
    <message>
        <location filename="../edit.cpp" line="108"/>
        <location filename="../edit.cpp" line="124"/>
        <source>Open Game Path</source>
        <translation>เปิดเส้นทางเกม</translation>
    </message>
    <message>
        <location filename="../edit.cpp" line="117"/>
        <source>Open AssaultCube</source>
        <translation>เปิด AssaultCube</translation>
    </message>
</context>
<context>
    <name>add</name>
    <message>
        <location filename="../add.ui" line="14"/>
        <source>Launcher Add</source>
        <translation>เริ่มต้นเพิ่ม</translation>
    </message>
    <message>
        <location filename="../add.ui" line="74"/>
        <source>Path</source>
        <translation>เส้นทาง</translation>
    </message>
    <message>
        <location filename="../add.ui" line="94"/>
        <source>Add</source>
        <translation>เพิ่ม</translation>
    </message>
    <message>
        <location filename="../add.ui" line="112"/>
        <source>Options:</source>
        <translation>ตัวเลือก:</translation>
    </message>
    <message>
        <location filename="../add.ui" line="130"/>
        <source>Name:</source>
        <translation>ชื่อ:</translation>
    </message>
</context>
<context>
    <name>edit</name>
    <message>
        <location filename="../edit.ui" line="14"/>
        <source>Launcher Edit</source>
        <translation>เริ่มต้นแก้ไข</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="74"/>
        <source>Path</source>
        <translation>เส้นทาง</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="94"/>
        <source>Save</source>
        <translation>บันทึก</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="112"/>
        <source>Options:</source>
        <translatorcomment>ชื่อ:</translatorcomment>
        <translation>ตัวเลือก:</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="130"/>
        <source>Name:</source>
        <translation>ชื่อ:</translation>
    </message>
</context>
<context>
    <name>launcher</name>
    <message>
        <location filename="../launcher.ui" line="14"/>
        <source>QT AssaultCube Launcher</source>
        <translation>QT AssaultCube เริ่มต้น</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="35"/>
        <source>Add +</source>
        <translation>เพิ่ม +</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="69"/>
        <source>Edit =</source>
        <translation>แก้ไข =</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="89"/>
        <source>Play &gt;</source>
        <translation>เล่น &gt;</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="106"/>
        <source>Chat on IRC</source>
        <translation>แชทใน IRC</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="126"/>
        <source>Delete -</source>
        <translation>ลบ -</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="449"/>
        <source>Sites</source>
        <translation>เว็บไซต์</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="457"/>
        <source>Documents</source>
        <translation>เอกสาร</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="464"/>
        <source>Content</source>
        <translation>เนื้อหา</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="471"/>
        <source>Packages</source>
        <translation>แพคเกจ</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="479"/>
        <source>About</source>
        <translation>เกี่ยวกับเรา</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="505"/>
        <source>Docs</source>
        <translation>เอกสาร</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="510"/>
        <source>Wiki</source>
        <translation>วิกิพีเดีย</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="515"/>
        <source>Gema Forums</source>
        <translation>ฟอรัม Gema</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="520"/>
        <source>Iguana Site</source>
        <translation>เว็บไซต์ Iguana</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="550"/>
        <source>Launcher</source>
        <translation>เริ่มต้น</translation>
    </message>
</context>
</TS>
