<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU" sourcelanguage="en">
<context>
    <name>Add</name>
    <message>
        <location filename="../add.cpp" line="81"/>
        <location filename="../add.cpp" line="90"/>
        <location filename="../add.cpp" line="97"/>
        <source>Open AssaultCube</source>
        <translation>Запустить AssaultCube</translation>
    </message>
</context>
<context>
    <name>Edit</name>
    <message>
        <location filename="../edit.cpp" line="108"/>
        <location filename="../edit.cpp" line="124"/>
        <source>Open Game Path</source>
        <translation>Открыть папку с игрой</translation>
    </message>
    <message>
        <location filename="../edit.cpp" line="117"/>
        <source>Open AssaultCube</source>
        <translation>Запустить AssaultCube</translation>
    </message>
</context>
<context>
    <name>add</name>
    <message>
        <location filename="../add.ui" line="14"/>
        <source>Launcher Add</source>
        <translation>Добавить Стартер</translation>
    </message>
    <message>
        <location filename="../add.ui" line="74"/>
        <source>Path</source>
        <translation>Путь</translation>
    </message>
    <message>
        <location filename="../add.ui" line="94"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../add.ui" line="112"/>
        <source>Options:</source>
        <translation>Опции:</translation>
    </message>
    <message>
        <location filename="../add.ui" line="130"/>
        <source>Name:</source>
        <translation>Название:</translation>
    </message>
</context>
<context>
    <name>edit</name>
    <message>
        <location filename="../edit.ui" line="14"/>
        <source>Launcher Edit</source>
        <translation>Изменить Стартер</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="74"/>
        <source>Path</source>
        <translation>Путь</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="94"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="112"/>
        <source>Options:</source>
        <translation>Опции:</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="130"/>
        <source>Name:</source>
        <translation>Название:</translation>
    </message>
</context>
<context>
    <name>launcher</name>
    <message>
        <location filename="../launcher.ui" line="14"/>
        <source>QT AssaultCube Launcher</source>
        <translation>QT AssaultCube Стартер</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="35"/>
        <source>Add +</source>
        <translation>Добавить +</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="69"/>
        <source>Edit =</source>
        <translation>Менять =</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="89"/>
        <source>Play &gt;</source>
        <translation>Играть &gt;</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="106"/>
        <source>Chat on IRC</source>
        <translation>IRC Чат</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="126"/>
        <source>Delete -</source>
        <translation>Удалить -</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="449"/>
        <source>Sites</source>
        <translation>Сайты</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="457"/>
        <source>Documents</source>
        <translation>Документация</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="464"/>
        <source>Content</source>
        <translation>Контент</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="471"/>
        <source>Packages</source>
        <translation>Пакеты</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="479"/>
        <source>About</source>
        <translation>О нас</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="505"/>
        <source>Docs</source>
        <translation>Документация</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="510"/>
        <source>Wiki</source>
        <translation>Википедия</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="515"/>
        <source>Gema Forums</source>
        <translation>Форумы GEMA</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="520"/>
        <source>Iguana Site</source>
        <translation>Сайт Iguana</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="550"/>
        <source>Launcher</source>
        <translation>Стартер</translation>
    </message>
</context>
</TS>
