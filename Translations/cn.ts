<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh" sourcelanguage="en">
<context>
    <name>Add</name>
    <message>
        <location filename="../add.cpp" line="81"/>
        <location filename="../add.cpp" line="90"/>
        <location filename="../add.cpp" line="97"/>
        <source>Open AssaultCube</source>
        <translation>运行 Assaultcube</translation>
    </message>
</context>
<context>
    <name>Edit</name>
    <message>
        <location filename="../edit.cpp" line="108"/>
        <location filename="../edit.cpp" line="124"/>
        <source>Open Game Path</source>
        <translation>开放游戏路径</translation>
    </message>
    <message>
        <location filename="../edit.cpp" line="117"/>
        <source>Open AssaultCube</source>
        <translation>运行 Assaultcube</translation>
    </message>
</context>
<context>
    <name>add</name>
    <message>
        <location filename="../add.ui" line="14"/>
        <source>Launcher Add</source>
        <translation>启动器添加</translation>
    </message>
    <message>
        <location filename="../add.ui" line="74"/>
        <source>Path</source>
        <translation>文件路</translation>
    </message>
    <message>
        <location filename="../add.ui" line="94"/>
        <source>Add</source>
        <translation>加</translation>
    </message>
    <message>
        <location filename="../add.ui" line="112"/>
        <source>Options:</source>
        <translation>设置:</translation>
    </message>
    <message>
        <location filename="../add.ui" line="130"/>
        <source>Name:</source>
        <translation>名称:</translation>
    </message>
</context>
<context>
    <name>edit</name>
    <message>
        <location filename="../edit.ui" line="14"/>
        <source>Launcher Edit</source>
        <translation>启动器编辑</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="74"/>
        <source>Path</source>
        <translation>文件路</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="94"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="112"/>
        <source>Options:</source>
        <translation>设置:</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="130"/>
        <source>Name:</source>
        <translation>名称:</translation>
    </message>
</context>
<context>
    <name>launcher</name>
    <message>
        <location filename="../launcher.ui" line="14"/>
        <source>QT AssaultCube Launcher</source>
        <translation>QT AssaultCube 启动器</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="35"/>
        <source>Add +</source>
        <translation>加 +</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="69"/>
        <source>Edit =</source>
        <translation>编辑 =</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="89"/>
        <source>Play &gt;</source>
        <translation>玩 &gt;</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="106"/>
        <source>Chat on IRC</source>
        <translation>上 IRC</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="126"/>
        <source>Delete -</source>
        <translation>删除 -</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="449"/>
        <source>Sites</source>
        <translation>网站</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="457"/>
        <source>Documents</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="464"/>
        <source>Content</source>
        <translation>目录</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="471"/>
        <source>Packages</source>
        <translation>包装</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="479"/>
        <source>About</source>
        <translation>关于我们</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="505"/>
        <source>Docs</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="510"/>
        <source>Wiki</source>
        <translation>维基</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="515"/>
        <source>Gema Forums</source>
        <translation>Gema 论坛</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="520"/>
        <source>Iguana Site</source>
        <translation>Iguana 网站</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="550"/>
        <source>Launcher</source>
        <translation>启动器</translation>
    </message>
</context>
</TS>
