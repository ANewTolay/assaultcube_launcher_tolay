<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bg" sourcelanguage="en">
<context>
    <name>Add</name>
    <message>
        <location filename="../add.cpp" line="81"/>
        <location filename="../add.cpp" line="90"/>
        <location filename="../add.cpp" line="97"/>
        <source>Open AssaultCube</source>
        <translation>Отвори AssaultCube</translation>
    </message>
</context>
<context>
    <name>Edit</name>
    <message>
        <location filename="../edit.cpp" line="108"/>
        <location filename="../edit.cpp" line="124"/>
        <source>Open Game Path</source>
        <translation>Отвори Път към Играта</translation>
    </message>
    <message>
        <location filename="../edit.cpp" line="117"/>
        <source>Open AssaultCube</source>
        <translation>Отвори AssaultCube</translation>
    </message>
</context>
<context>
    <name>add</name>
    <message>
        <location filename="../add.ui" line="14"/>
        <source>Launcher Add</source>
        <translation>Стартер Добави</translation>
    </message>
    <message>
        <location filename="../add.ui" line="74"/>
        <source>Path</source>
        <translation>Път</translation>
    </message>
    <message>
        <location filename="../add.ui" line="94"/>
        <source>Add</source>
        <translation>Добави</translation>
    </message>
    <message>
        <location filename="../add.ui" line="112"/>
        <source>Options:</source>
        <translation>Настроики:</translation>
    </message>
    <message>
        <location filename="../add.ui" line="130"/>
        <source>Name:</source>
        <translation>Име:</translation>
    </message>
</context>
<context>
    <name>edit</name>
    <message>
        <location filename="../edit.ui" line="14"/>
        <source>Launcher Edit</source>
        <translation>Стартер Промени</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="74"/>
        <source>Path</source>
        <translation>Път</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="94"/>
        <source>Save</source>
        <translation>Запази</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="112"/>
        <source>Options:</source>
        <translation>Настроики:</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="130"/>
        <source>Name:</source>
        <translation>Име:</translation>
    </message>
</context>
<context>
    <name>launcher</name>
    <message>
        <location filename="../launcher.ui" line="14"/>
        <source>QT AssaultCube Launcher</source>
        <translation>QT AssaultCube Стартер</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="35"/>
        <source>Add +</source>
        <translation>Добави +</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="69"/>
        <source>Edit =</source>
        <translation>Промени =</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="89"/>
        <source>Play &gt;</source>
        <translation>Играй &gt;</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="106"/>
        <source>Chat on IRC</source>
        <translation>Чат в ИРЦ</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="126"/>
        <source>Delete -</source>
        <translation>Изтрий -</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="405"/>
        <source>Sites</source>
        <translation>Сайтове</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="413"/>
        <source>Documents</source>
        <translation>Документация</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="420"/>
        <source>Content</source>
        <translation>Съдържание</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="427"/>
        <source>Packages</source>
        <translation>Пакети</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="435"/>
        <source>About</source>
        <translation>За нас</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="453"/>
        <source>Docs</source>
        <translation>Документи</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="458"/>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="463"/>
        <source>Gema Forums</source>
        <translation>GEMA Форуми</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="468"/>
        <source>Iguana Site</source>
        <translation>Iguana Сайт</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="498"/>
        <source>Launcher</source>
        <translation>Стартер</translation>
    </message>
</context>
</TS>
