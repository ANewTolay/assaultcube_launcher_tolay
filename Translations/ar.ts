<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ar" sourcelanguage="en">
<context>
    <name>Add</name>
    <message>
        <location filename="../add.cpp" line="81"/>
        <location filename="../add.cpp" line="90"/>
        <location filename="../add.cpp" line="97"/>
        <source>Open AssaultCube</source>
        <translation>فتح AssaultCube</translation>
    </message>
</context>
<context>
    <name>Edit</name>
    <message>
        <location filename="../edit.cpp" line="108"/>
        <location filename="../edit.cpp" line="124"/>
        <source>Open Game Path</source>
        <translation>فتح مسار اللعبة</translation>
    </message>
    <message>
        <location filename="../edit.cpp" line="117"/>
        <source>Open AssaultCube</source>
        <translation>فتح AssaultCube</translation>
    </message>
</context>
<context>
    <name>add</name>
    <message>
        <location filename="../add.ui" line="14"/>
        <source>Launcher Add</source>
        <translation>إضافة مشغل</translation>
    </message>
    <message>
        <location filename="../add.ui" line="74"/>
        <source>Path</source>
        <translation>مسار</translation>
    </message>
    <message>
        <location filename="../add.ui" line="94"/>
        <source>Add</source>
        <translation>إضافة</translation>
    </message>
    <message>
        <location filename="../add.ui" line="112"/>
        <source>Options:</source>
        <translation>:خيارات</translation>
    </message>
    <message>
        <location filename="../add.ui" line="130"/>
        <source>Name:</source>
        <translation>:اسم</translation>
    </message>
</context>
<context>
    <name>edit</name>
    <message>
        <location filename="../edit.ui" line="14"/>
        <source>Launcher Edit</source>
        <translation>تحرير مشغل</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="74"/>
        <source>Path</source>
        <translation>مسار</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="94"/>
        <source>Save</source>
        <translation>حفظ</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="112"/>
        <source>Options:</source>
        <translation>:خيارات</translation>
    </message>
    <message>
        <location filename="../edit.ui" line="130"/>
        <source>Name:</source>
        <translation>:اسم</translation>
    </message>
</context>
<context>
    <name>launcher</name>
    <message>
        <location filename="../launcher.ui" line="14"/>
        <source>QT AssaultCube Launcher</source>
        <translation> QT AssaultCube مشغل</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="35"/>
        <source>Add +</source>
        <translation>إضافة +</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="69"/>
        <source>Edit =</source>
        <translation>تحرير =</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="89"/>
        <source>Play &gt;</source>
        <translation>لعب &gt;</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="106"/>
        <source>Chat on IRC</source>
        <translation>IRC الدردشة على</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="126"/>
        <source>Delete -</source>
        <translation>حذف -</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="449"/>
        <source>Sites</source>
        <translation>المواقع</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="457"/>
        <source>Documents</source>
        <translation>مستندات</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="464"/>
        <source>Content</source>
        <translation>المحتوى</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="471"/>
        <source>Packages</source>
        <translation>الحزم</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="479"/>
        <source>About</source>
        <translation>من نحن</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="505"/>
        <source>Docs</source>
        <translation>محرر المستندات</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="510"/>
        <source>Wiki</source>
        <translation>ويكي</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="515"/>
        <source>Gema Forums</source>
        <translation>Gema منتديات</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="520"/>
        <source>Iguana Site</source>
        <translation>Iguana موقع</translation>
    </message>
    <message>
        <location filename="../launcher.ui" line="550"/>
        <source>Launcher</source>
        <translation>مشغل</translation>
    </message>
</context>
</TS>
