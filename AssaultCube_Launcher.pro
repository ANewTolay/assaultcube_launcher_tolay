#-------------------------------------------------
#
# Project created by QtCreator 2017-02-08T18:37:43
#
#-------------------------------------------------

QT       += core xml network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AssaultCube_Launcher
TEMPLATE = app


SOURCES += main.cpp\
        launcher.cpp \
    add.cpp \
    edit.cpp

TRANSLATIONS += Translations/es.ts \
    Translations/pt.ts \
    Translations/de.ts \
    Translations/fr.ts \
    Translations/nl.ts \
    Translations/it.ts \
    Translations/se.ts \
    Translations/tr.ts \
    Translations/ru.ts \
    Translations/cn.ts \
    Translations/jp.ts \
    Translations/kr.ts \
    Translations/th.ts \
    Translations/in.ts \
    Translations/my.ts \
    Translations/il.ts \
    Translations/ar.ts \
    Translations/fa.ts \
    Translations/za.ts

HEADERS  += launcher.h \
    add.h \
    edit.h

FORMS    += launcher.ui \
    add.ui \
    edit.ui

QMAKE_LFLAGS += -Wl,-rpath,"'\$$ORIGIN'"

RESOURCES += \
    resources.qrc

DISTFILES +=

target.path += build/
target.files += $$TARGET \
	./Deployment/PACKAGING/README.html \
	./Deployment/PACKAGING/icon.png

linux {
	target.files += ./Deployment/PACKAGING/Luncher.desktop
}

INSTALLS += target
