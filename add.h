#ifndef ADD_H
#define ADD_H

#include <QDialog>

namespace Ui {
class add;
}

class Add : public QDialog
{
    Q_OBJECT

public:
    QStringList *passedNameOfProgram;
    QStringList *passedPath;
    QStringList *passedCommands;
    explicit Add(QWidget *parent = 0);
    void setInputStrings(QStringList &game,QStringList &stringPath,QStringList &stingCommand);
    void writeConfigFile();
    ~Add();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_6_clicked();

    void on_lineEdit_5_textChanged();

    void on_lineEdit_4_textChanged();

private:
    Ui::add *ui;
};

#endif // ADD_H
