# README #

AssaultCube (cube engine) game launcher made with QT4 and QT5. 

###### Image
![alt text](http://hamster.bgx.ro/~iguana/images/launcher.png "Launcher")

###### Quick summary
QT Creator project files and packaging used to create a launcher for AssaultCube related games.

###### Version
1.2 beta

##### **Downloads and Updates**
**[BitBucket](https://bitbucket.org/iguanameow/assaultcube_launcher/downloads/)** | **[Yandex](https://yadi.sk/d/3gfAhQr03GRSwm)** | **[Google](https://drive.google.com/open?id=0B89jERR-fWR9aXFqRlR2QUtGUGc)**

###### TO DO
1. Translations to other languages: japanese, thai, turkish, hebrew.
2. Impliment update to check for new version (in progress).


### How do I get set up? ###

###### Requierments
+ Copy of the AssaultCube game or its forks, 
+ Standalone [QT 4.8.6](https://download.qt.io/archive/qt/) or above and [QT Creator 3.0.1](https://download.qt.io/archive/qtcreator/) or above, or a compatible [Qt online installer](https://download.qt.io/archive/online_installers/), 
+ [MinGW](http://mingw.org/) for Windows,
+ [7-zip](http://7-zip.org/) for Windows, [Keka](http://www.kekaosx.com/) for OSX, and p7zip for Linux.

###### System Requierments
Windows 2000 32-bit or higher, Linux 32/64-bit, OSX 64-bit.

###### Testing & Deployment
1. Qmake before building project, 
2. Build and Run the project, 
3. Release must be **[packaged](https://bitbucket.org/iguanameow/assaultcube_launcher/src/f8215ddf8cf8d87d752b1d11feee8c4c9a42ed2d/Deployment/PACKAGING/)** to include binary and libraries. See **[deployment](https://bitbucket.org/iguanameow/assaultcube_launcher/src/c4697bb04b66d85b898ae36eab6bb966ba01aa14/Deployment/)** directory of this project.

###### Extracting
+ Use 7-zip's program interface in Windows and OSX to extract. 
+ For Linux either either use an interface or perform this command:
```
7z x Linux64_AC_Launcher_v1.1.7z
```

###### SHA1 Hashes
```
c629d2a7d1ac0e0da65e20d0256d86bdf6a9fadd  Linux32_AC_Launcher_v1.2b.7z
cdac6683332ffcb96099ecaf1fedf4f5710e6653  Linux64_AC_Launcher_v1.2b.7z
e21443cccdea5419ffe20992828b6a0ab91c08da  OSX64_AC_Launcher_v1.2b.7z
d8069acc02241a2041b34cd39018b8ea6ba370e3  Win32_AC_Launcher_v1.2b.7z

```

### Who do I talk to? ###

###### Contact
Repo owner or contributing project members.
