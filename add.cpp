#include "add.h"
#include "ui_add.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>

Add::Add(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::add)
{
    ui->setupUi(this);
    if(ui->lineEdit_5->text().size()>0 && ui->lineEdit_4->text().size()>0)
    {
        ui->pushButton_2->setEnabled(true);
    }else{
        ui->pushButton_2->setEnabled(false);
    }
}

void Add::setInputStrings(QStringList &game,QStringList &stringPath,QStringList &stingCommand)  // Recieve data from parent
{
    passedNameOfProgram=&game;
    passedPath=&stringPath;
    passedCommands=&stingCommand;
}

void Add::writeConfigFile()    // Write back to config file
{
    QFile file("ConfigFile.txt");
    file.open(QIODevice::WriteOnly);
    QTextStream out(&file);
    out << "$,numCases,"+QString::number((*passedNameOfProgram).size()) << endl; // Writing header
    for(int i=0;i<(*passedNameOfProgram).size();i++)    // Rewrite all data
    {
        out << QString::number(i)+",name,"+(*passedNameOfProgram)[i] << endl;
        out << QString::number(i)+",path,"+(*passedPath)[i] << endl;
        out << QString::number(i)+",command,"+(*passedCommands)[i] << endl;
    }
    file.close();
}

Add::~Add()
{
    delete ui;
}

void Add::on_pushButton_2_clicked() // Add Button
{
    qDebug("Adding to Config File");
    (*passedNameOfProgram).append(ui->lineEdit_5->text());
    (*passedPath).append(ui->lineEdit_4->text());
    (*passedCommands).append(ui->lineEdit_3->text());
    writeConfigFile();
    close();
}

void Add::on_lineEdit_5_textChanged()
{
    if(ui->lineEdit_5->text().size()>0 && ui->lineEdit_4->text().size()>0)
    {
        ui->pushButton_2->setEnabled(true);
    }else{
        ui->pushButton_2->setEnabled(false);
    }
}

void Add::on_lineEdit_4_textChanged()
{
    if(ui->lineEdit_5->text().size()>0 && ui->lineEdit_4->text().size()>0)
    {
        ui->pushButton_2->setEnabled(true);
    }else{
        ui->pushButton_2->setEnabled(false);
    }
}

void Add::on_pushButton_6_clicked() // Choose Path Button
{
qDebug("Opening Path");
#ifdef Q_OS_MACOS
    QString filename = QFileDialog::getOpenFileName(this,tr("Open AssaultCube"), "/Applications", "app files (*.app);; All files (*)");
    if(filename.isNull())
    {
        filename="";
    }else{
        filename=filename+"/Contents/MacOS/launcher";
    }
    ui->lineEdit_4->setText(filename);
#elif _WIN32
    QString filename = QFileDialog::getOpenFileName(this,tr("Open AssaultCube"), "C://", "bat files (*.bat);; exe files (*.exe);; All files (*)");
    if(filename.isNull())
        {
          filename="";
        }
    ui->lineEdit_4->setText(filename);
#else
    QString filename = QFileDialog::getOpenFileName(this,tr("Open AssaultCube"), "/home", "shell files (*.sh);; All files (*)");
    if(filename.isNull())
        {
          filename="";
        }
    ui->lineEdit_4->setText(filename);
#endif
}
