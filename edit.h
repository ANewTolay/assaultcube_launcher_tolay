#ifndef EDIT_H
#define EDIT_H

#include <QDialog>

namespace Ui {
class edit;
}

class Edit : public QDialog
{
    Q_OBJECT

public:
    QStringList *passedNameOfProgram;
    QStringList *passedPath;
    QStringList *passedCommands;
    int passedSelection;
    explicit Edit(QWidget *parent = 0);
    void setInputStrings(QStringList &game,QStringList &stringPath,QStringList &stingCommand,int selection);
    void writeConfigFile();
    ~Edit();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_6_clicked();

    void on_lineEdit_5_textChanged();

    void on_lineEdit_4_textChanged();

private:
    Ui::edit *ui;
};

#endif // EDIT_H
